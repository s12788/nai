import heapq
from cell import *

# Based on https://www.laurentluce.com/posts/solving-mazes-using-python-simple-recursivity-and-a-search/


class A_star(object):
    def __init__(self):
        self.opened = []            # lista otwarta
        heapq.heapify(self.opened)
        self.closed = set()         # lista zamknieta - odwiedzone komorki
        self.cells = []             # lista komorek grida

    def init_grid(self, grid_size, walls, start, end):

        self.grid_size = grid_size
        for x in range(self.grid_size):
            for y in range(self.grid_size):                 #
                if (x, y) in walls:                         # grid jako lista
                    wall = False                            #
                else:
                    wall = True
                self.cells.append(Cell(x, y, wall))
        self.start = self.get_cell(start[0], start[1])
        self.end = self.get_cell(end[0], end[1])

    def get_heuristic(self, cell):

        return 0.5*abs(cell.x - self.end.x) + abs(cell.y - self.end.y)      # heurystyka manhattan

    def get_cell(self, x, y):

        return self.cells[x * self.grid_size + y]

    def get_neighbours(self, cell):

        cells = []
        if cell.x < self.grid_size-1:
            cells.append(self.get_cell(cell.x+1, cell.y))
        if cell.x < self.grid_size-1 and cell.y > 0:
			cells.append(self.get_cell(cell.x+1, cell.y-1))
        if cell.y > 0:
            cells.append(self.get_cell(cell.x, cell.y-1))       #
        if cell.y > 0 and cell.x > 0:
			cells.append(self.get_cell(cell.x-1, cell.y-1))
        if cell.x > 0:                                          # zwraca sasiednie komorki, od prawej w dol
            cells.append(self.get_cell(cell.x-1, cell.y))       #
        if cell.x > 0 and cell.y < self.grid_size-1:
			cells.append(self.get_cell(cell.x-1, cell.y+1))
        if cell.y < self.grid_size-1:
            cells.append(self.get_cell(cell.x, cell.y+1))
        if cell.y < self.grid_size-1 and cell.x < self.grid_size-1:
			cells.append(self.get_cell(cell.x+1, cell.y+1))
        return cells

    def get_path(self):
        cell = self.end
        path = [(cell.x, cell.y)]
        while cell.parent is not self.start:
            cell = cell.parent
            path.append((cell.x, cell.y))

        path.append((self.start.x, self.start.y))
        path.reverse()
        return path

    def update_neighbour(self, neighbour, cell):

        neighbour.g = cell.g + 1
        neighbour.h = self.get_heuristic(neighbour)
        neighbour.parent = cell
        neighbour.f = neighbour.h + neighbour.g

    def find(self):

        heapq.heappush(self.opened, (self.start.f, self.start))     # komorka start do kopca

        while len(self.opened):                                     # dopoki lista otwarta nie jest pusta
            f, cell = heapq.heappop(self.opened)                    # zdejmij komorke z kopca

            self.closed.add(cell)                                   # dodanie komorki do listy zamknietej
            if cell is self.end:                                    # jesli komorka koncowa zwroc sciezke
                return self.get_path()
            neighbours = self.get_neighbours(cell)                  # wez sasiednie komorki
            for neighbour in neighbours:
                if neighbour.wall and neighbour not in self.closed:
                    if (neighbour.f, neighbour) in self.opened:                     # jesli sasiednia komorka na otwartej, sprawdz czy sciezka lepsza od poprzednio znalezionej
                        if neighbour.g > cell.g + 1:
                            self.update_neighbour(neighbour, cell)
                    else:
                        self.update_neighbour(neighbour, cell)                        # sasiednia komorka do otwartej
                        heapq.heappush(self.opened, (neighbour.f, neighbour))

    # def __init__(self, grid_size, walls, start, end):


    def resolve(self, grid_size, walls, start, end):
        astar = A_star()
        astar.init_grid(grid_size, walls, start, end)
        path = astar.find()
        return path





