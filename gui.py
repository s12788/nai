import Tkinter
from Tkinter import *
from astar import *


class Maze(object):


    def __init__(self, grid_size):

        self.x = grid_size
        self.y = grid_size
        self.buttons = [[object] * self.x for i in range(self.y)]
        self.walls = []
        self.s = 0
        self.e = 0
        self.init_maze()

    def init_maze(self):

        root = Tkinter.Tk()
        Tkinter.Grid.rowconfigure(root, 0, weight=1)
        Tkinter.Grid.columnconfigure(root, 0, weight=1)
        self.frame = Tkinter.Frame(root)
        self.frame.grid(row=0, column=0, sticky=N + S + E + W)
        root.title("A*")

        for x in range(self.x):
            for y in range(self.y):
                Grid.columnconfigure(self.frame, y, weight=1)
                self.buttons[x][y] = Tkinter.Button(self.frame, text=" ", borderwidth=1, command=lambda x=x, y=y: self.switch_button(x, y), height=5, width=7)
                self.buttons[x][y].grid(row=x, column=y, sticky=N + S + E + W)
        self.find_button = Button(self.frame, text="FIND", borderwidth=3, command=lambda: self.find()).grid(row=x+1, column=0, columnspan=2, sticky=N + S + E + W)
        self.reset_button = Button(self.frame, text="RESET",borderwidth=3, command=lambda: self.reset()).grid(row=x+1, column=y-1, columnspan=2, sticky=N + S + E + W)
        self.default_color = self.buttons[0][0].cget("background")

        root.mainloop()



    def switch_button(self, x, y):

        if self.buttons[x][y]["text"] == " "  and self.s == 0:
            self.buttons[x][y]["text"] = "S"
            self.buttons[x][y]["bg"] = "red"
            self.s = 1
            self.start = (x,y)
        elif self.buttons[x][y]["text"] == "S"  and self.s == 1 and self.e == 0:
            self.buttons[x][y]["text"] = "E"
            self.buttons[x][y]["bg"] = "red"
            self.s = 0
            self.e = 1
            self.end = (x,y)
        elif self.buttons[x][y]["text"] == "E"  and self.e == 1 and self.s == 0:
            self.buttons[x][y]["text"] = " "
            self.buttons[x][y]["bg"] = self.default_color
            self.e = 0
        elif self.buttons[x][y]["text"] == " " and self.s == 1 and self.e ==0:
            self.buttons[x][y]["text"] = "E"
            self.buttons[x][y]["bg"] = "red"
            self.s = 1
            self.e = 1
            self.end = (x, y)
        elif self.buttons[x][y]["text"] == "E" and self.e == 1 and self.s == 1:
            self.buttons[x][y]["text"] = " "
            self.buttons[x][y]["bg"] = self.default_color
            self.s = 1
            self.e = 0
        elif self.buttons[x][y]["text"] == "S" and self.s == 1 and self.e == 1:
            self.buttons[x][y]["text"] = " "
            self.buttons[x][y]["bg"] = self.default_color
            self.s = 0
            self.e = 1
        elif self.buttons[x][y]["text"] == " " and self.e == 1 and self.s == 1:
            self.buttons[x][y]["text"] = "W"
            self.buttons[x][y]["bg"] = "black"
            self.walls.append((x,y))

        elif self.buttons[x][y]["text"] == "W" and self.e == 1 and self.s == 1:
            self.buttons[x][y]["text"] = " "
            self.buttons[x][y]["bg"] = self.default_color
            self.walls.remove((x,y))


    def find(self):

        if self.s == 1 and self.e == 1:
            astar = A_star()
            path = astar.resolve(self.x, self.walls, self.start, self.end)
            if path == None:
                Label(self.frame, text="Endpoint is unreachable!", borderwidth=0, fg="red").grid(row=self.x, column=self.y - 8, columnspan = 6, sticky=N + S + E + W)
                return
            for x in range(self.x):
                for y in range(self.y):
                    if (x, y) in path and (x,y) != self.start and (x,y) != self.end:
                        self.buttons[x][y] = Tkinter.Button(self.frame, bg="green", borderwidth=1, command=lambda x=x, y=y: self.switch_button(x, y), height=5, width=7)
                        self.buttons[x][y].grid(row=x, column=y, sticky=N + S + E + W)
                        Label(self.frame, text="", borderwidth=0).grid(row=self.x, column=self.y - 8, columnspan=6, sticky=N + S + E + W)

        else:
            Label(self.frame, text="Set start and end point!", borderwidth=0, fg="red").grid(row=self.x,
                                                                                             column=self.y - 8,
                                                                                             columnspan=6,
                                                                                             sticky=N + S + E + W)


    def reset(self):

        for x in range(self.x):
            for y in range(self.y):
                if self.buttons[x][y]["bg"] == "green":
                    self.buttons[x][y]["text"] = " "
                    self.buttons[x][y]["bg"] = self.default_color



Maze(10).init_maze